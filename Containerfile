# syntax=docker/dockerfile:1.3
FROM docker.io/immawanderer/fedora-minimal:linux-amd64

ARG BUILD_DATE
ARG VCS_REF

LABEL description="Container image for building Fyne (fyne.io) Go projects"

LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-url="https://git.dotya.ml/wanderer-containers/fedora-go-fyne.git" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.license=GPL-3.0

# DL3041 warning: Specify version with `dnf install -y <package>-<version>`.
RUN microdnf install -y \
      --nodocs \
      --setopt install_weak_deps=0 \
      git \
      golang \
      gcc \
      glibc-headers \
      libXcursor-devel \
      libXrandr-devel \
      mesa-libGL-devel \
      libXi-devel \
      libXinerama-devel \
      libXxf86vm-devel \
    && \
    \
    microdnf clean all -y; \
    \
    rm -rf /usr/share/zoneinfo/*; \
    find /. -name "*~" -type f -delete > /dev/null 2>&1; \
    find /usr/share/terminfo/. -type f -delete \
      ! -name "*xterm*" ! -name "*screen*" ! -name "*screen*"

WORKDIR /

# vim: ft=dockerfile
